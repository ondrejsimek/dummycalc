package evaluating;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Parser ma na starosti spocitat cely priklad
 * 
 * @author angel333
 */
public class Parser
{
	public static final String REGEXP_REF = "(N\\d+)";
	/**
	 * Simplifier obsahuje jednoduche metody pro zjednodusovani
	 * 
	 * V nasledujicim kodu je pouzito par shortucutu:
	 * s() - prevod na String
	 * n() - prevod na Double
	 * m   - trida Matcher pro aktualni regularni vyraz
	 * 
	 * @author angel333
	 */
	public static class Simplifier
	{
		/**
		 * List cisel a jejich referenci
		 * 
		 * Protoze zjednodusovani probiha vramci stringu, a do stringu je
		 * problematicke davati doubly.. Jsou tedy ukladany jen reference...
		 * 
		 * Ve stringu s prikladem cislo vypada zhruba jako N123, pricemz 123 je
		 * poradi v numberRefs.
		 */
		public static List<Double> numberRefs;
		
		
		/**
		 * Takhle ma vypadat resitelska trida..
		 * 
		 * @author angel333
		 */
		private interface Solver
		{
			public String getRegex();
			public String run(Matcher matcher);
		}
		
		
		/**
		 * Zavola resitele a vysledek sikovne obali zbytkem prikladu..
		 * 
		 * @param regex
		 * @param solver
		 * @param problem
		 * @return
		 */
		private static String wrapper(Solver solver, String problem)
		{
			Matcher matcher = Pattern.compile(solver.getRegex()).matcher(problem);
			while (matcher.find())
			{
				// Zjednodusime problem..
				problem =
						problem.substring(0, matcher.start()) +
						solver.run(matcher) +
						problem.substring(matcher.end());
				
				// .. a resetujeme matcher..
				matcher = matcher.pattern().matcher(problem);
			}
			return problem;
		}
		
		
		/**
		 * Nacte cislo podle reference
		 * 
		 * @param a
		 * @return
		 */
		public static Double n (String a)
		{
			//return String.valueOf(numberRefs.size());
			
			if (a.startsWith("N"))
				a = a.substring(1);
			
			return numberRefs.get(Integer.valueOf(a));
			//return Double.valueOf(a);
		}
		

		/**
		 * Ulozi cislo do reference a tu vrati
		 * 
		 * @param a
		 * @return
		 */
		public static String s (Double a)
		{
			numberRefs.add(a);
			return "N" + String.valueOf(numberRefs.size() - 1);
			//return String.valueOf(a);
		}
		
		
		/**
		 * ZAVORKY ! -> HURA NA REKURZI !!!
		 *
		 * @param problem
		 * @return
		 */
		public static String brackets(String problem)
		{
			return wrapper(new Solver() {
				public String getRegex() {
					return "\\(([N0-9\\.\\+\\-\\*\\/]+)\\)";
				}
				public String run(Matcher m) {
					return Parser.simplify(m.group(1));
					//return evaluate(m.group(1));
				}
			}, problem);
		}
		
		
		/**
		 * Scitani a odcitani, resp. spis takove "hromadeni"
		 * 
		 * Nepouziva wrapper, ani Solver.. Scitani a odcitani ma trochu jinou
		 * filozofii nez ostatni operace..
		 * 
		 * @param problem
		 * @return
		 */
		public static String adding(String problem)
		{
			Matcher m = Pattern.compile
					("([\\+\\-]+)?" + REGEXP_REF).matcher(problem);
			double result = 0;
			
			while (m.find()) {
				if (m.group(1) == null)
					result += n(m.group(2));
				else // mnohonasobne minusy
					if (m.group(1).replace("+", "").length() % 2 == 0)
						result += n(m.group(2));
					else
						result -= n(m.group(2));
			}
			
			return s(result);
		}

		
		/**
		 * Nasobeni
		 * 
		 * @param problem
		 * @return
		 */
		public static String multiplying(String problem)
		{
			return wrapper(new Solver() {
				public String getRegex() {
					return REGEXP_REF + "\\*" + REGEXP_REF;
				}
				public String run(Matcher m) {
					return s(n(m.group(1)) * n(m.group(2)));
				}
			}, problem);
		}


		/**
		 * Deleni
		 * 
		 * @param problem
		 * @return
		 */
		public static String dividing(String problem)
		{
			return wrapper(new Solver() {
				public String getRegex() {
					return REGEXP_REF + "\\/" + REGEXP_REF;
				}
				public String run(Matcher m) {
					return s(n(m.group(1)) / n(m.group(2)));
				}
			}, problem);
		}


		/**
		 * Mocniny
		 * 
		 * @param problem
		 * @return
		 */
		public static String powering(String problem)
		{
			return wrapper(new Solver() {
				public String getRegex() {
					return REGEXP_REF + "\\^" + REGEXP_REF;
				}
				public String run(Matcher m) {
					return s(Math.pow(
							n(m.group(1)), n(m.group(2))
					));
				}
			}, problem);
		}
		
		
		/**
		 * Sinus
		 * 
		 * @param problem
		 * @return
		 */
		public static String sinus(String problem)
		{
			return wrapper(new Solver() {
				public String getRegex() {
					return "sin" + REGEXP_REF;
				}
				public String run(Matcher m) {
					return s(Math.sin(n(m.group(1))));
				}
			}, problem);
		}

		
		/**
		 * Cosinus
		 * 
		 * @param problem
		 * @return
		 */
		public static String cosinus (String problem)
		{
			return wrapper(new Solver() {
				public String getRegex() {
					return "cos" + REGEXP_REF;
				}
				public String run(Matcher m) {
					return s(Math.cos(n(m.group(1))));
				}
			}, problem);
		}
		
		
		/**
		 * Tangens
		 * 
		 * @param problem
		 * @return
		 */
		public static String tangens (String problem)
		{
			return wrapper(new Solver() {
				public String getRegex() {
					return "tan" + REGEXP_REF;
				}
				public String run(Matcher m) {
					return s(Math.tan(n(m.group(1))));
				}
			}, problem);
		}
	}
	
	
	/**
	 * Pocita priklad se vsim vsudy. Ma na starosti priority.
	 * 
	 * @param problem
	 * @return
	 */
	public static Double evaluate (String problem, double x)
	{
		Simplifier.numberRefs = new ArrayList<Double>();
		
		problem = fillX(problem, x);
		problem = convertNumbersToRefs(problem);
		problem = tidy(problem);
		problem = fillMissingMultiplications(problem);
		
		//if (!Parser.validateBrackets(problem))
			// TODO dodelat vyjimku!
			//return "CHYBA";
		
		problem = simplify(problem);
		return Simplifier.n(problem);
	}
	
	
	/**
	 * Samotne zjednodusovani.. Ale pouzitelne i pro zavorky, atd..
	 * Tj. neinicializuje reference..
	 * 
	 * @param problem
	 * @return
	 */
	public static String simplify (String problem)
	{
		problem = Simplifier.brackets(problem);
		problem = Simplifier.sinus(problem);
		problem = Simplifier.cosinus(problem);
		problem = Simplifier.tangens(problem);
		problem = Simplifier.powering(problem);
		problem = Simplifier.dividing(problem);
		problem = Simplifier.multiplying(problem);
		problem = Simplifier.adding(problem);
		
		return problem;
	}
	
	
	/**
	 * Varianta evaluate bez X
	 * 
	 * @param problem
	 * @return
	 */
	public static Double evaluate (String problem)
	{
		return evaluate(problem, 0); // treba nula.. to je jedno..
	}

	
	/**
	 * Doplni X
	 * 
	 * @param problem
	 * @param x
	 * @return
	 */
	public static String fillX(String problem, double x) {
		return problem.replaceAll("[xX]", "(" + String.valueOf(x) + ")");
	}


	/**
	 * Zkontroluje, zda jsou spravne zavorky
	 * 
	 * @param problem
	 * @return
	 */
	public static boolean validateBrackets (String problem)
	{
		return
				problem.replaceAll("[^\\(]", "").length() ==
				problem.replaceAll("[^\\)]", "").length();
	}
	
	
	/**
	 * Prevede zavorky na stejny format, vymaze mezery
	 * 
	 * @param problem
	 * @return
	 */
	public static String tidy (String problem)
	{
		return problem.
				replaceAll("[\\[\\{]", "(").
				replaceAll("[\\]\\}]", ")").
				replaceAll(" ", "");
	}
	
	
	/**
	 * Doplni chybejici nasobitka
	 * 
	 * @param problem
	 * @return
	 */
	public static String fillMissingMultiplications (String problem)
	{
		return problem.
				replaceAll("(\\d\\.)\\(", "$1*(").
				replaceAll("\\)(\\d\\.)", ")*$1");
	}
	
	
	/**
	 * Prevede vsechna cisla na reference
	 * 
	 * @param problem
	 * @return
	 */
	public static String convertNumbersToRefs (String problem)
	{
		// Nasleduje pouha magie!! :)
		
		Matcher m = Pattern.compile
				("[\\+\\-]?[\\d\\.]+").matcher(problem);
		// vice minusu
		
		String problem2 = "";
		Integer lastOffset = 0;
		Double val;
		Integer index;
		
		while (m.find())
		{
			val = Double.valueOf(m.group(0));
			Simplifier.numberRefs.add(val);
			index = Simplifier.numberRefs.size() - 1;
			
			problem2 += problem.substring(lastOffset, m.start());
			problem2 += "N" + index;
			lastOffset = m.end();
		}
		
		problem2 += problem.substring(lastOffset);
		
		return problem2;
	}
}
