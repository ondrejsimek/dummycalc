package evaluating;

import static org.junit.Assert.*;

import org.junit.Test;

public class ParserTest {

	@Test
	public void test() {
		
		// Neni inicializovanej list..
		//assertEquals("+0+1+2+3+", Parser.convertNumbersToRefs("+20+30+40+50+"));
		
		assertEquals(4.0, Parser.evaluate("2+2"), 0);
		
		
		assertEquals(2.0, Parser.evaluate("4/2"), 0);


		// Par prikladku
		assertEquals(0.0, Parser.evaluate("1-1"), 0);
		assertEquals(10.0, Parser.evaluate("2*5"), 0);
		assertEquals(4.0, Parser.evaluate("2+2"), 0);
		assertEquals(28.0, Parser.evaluate("3+(4+(3-1.0+2))+(6+(7+3)+1)"), 0);
		assertEquals(150.0, Parser.evaluate("3*10/2*10"), 0); // prednost deleni
		assertEquals(121.0, Parser.evaluate("11^2"), 0);

		// Validace zavorek
		assertFalse(Parser.validateBrackets("((())"));
		assertFalse(Parser.validateBrackets("(()))"));
		assertFalse(Parser.validateBrackets("))"));
		assertFalse(Parser.validateBrackets("("));
		assertTrue(Parser.validateBrackets("()"));
		assertTrue(Parser.validateBrackets("(()())"));
		assertTrue(Parser.validateBrackets("((()()))"));
		assertTrue(Parser.validateBrackets("()()()"));
		
		// Uklizeni prikladu
		assertEquals("(())", Parser.tidy("[  {]   } ")); // sezere vse! :)
		
		// Doplneni nasobitek
		//assertEquals("3*(4)", Parser.fillMissingMultiplications("3(4)"));
		//assertEquals("3*(4)*5", Parser.fillMissingMultiplications("3(4)5"));
		//assertEquals("1*(2*(3*(4*(5)*6)*7)*8)*9",
		//		Parser.fillMissingMultiplications("1(2(3(4(5)6)7)8)9"));
		
		// Minusy
		assertEquals(1.0, Parser.evaluate("-(-1)"), 0);
		assertEquals(1.0, Parser.evaluate("(-1)*(-1)"), 0);
		
		assertEquals(3.0, Parser.evaluate("1+1*2)"), 0);
		assertEquals(2.0, Parser.evaluate("1+1*2/2)"), 0);
		
		
		// Slozitejsi
		assertEquals(1.0, Parser.evaluate("----1"), 0);
		
		// Prekerni priklad od zemly
		assertEquals(2.0, Parser.evaluate("1*(1+1)"), 0);
		assertEquals(-2021.0, Parser.evaluate("‎(10-4*(4*4-(4*(2*4*(4-1*(4*5-2)))+(4*2-52)))+1)"), 0);
		assertEquals(-8840.0, Parser.evaluate("‎4*(4*5*(4*4-42*(4-1)-1)+10)"), 0);
		
		// Velke cislo
		assertEquals(1E99, Parser.evaluate("0.1*10^100"), 1.2142E83);
	}
	
}
