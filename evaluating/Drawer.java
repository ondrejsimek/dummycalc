package evaluating;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JLabel;


/**
 * Vykresluje grafy
 * 
 * @author angel333
 */
public class Drawer extends JLabel implements ComponentListener
{
	private static final long serialVersionUID = 1L;
	public BufferedImage image;
	public Graphics2D g;
	
	
	/**
	 * Jen inicializujeme platno..
	 */
	public Drawer ()
	{
		addComponentListener(this);
	}
	
	
	/**
	 * Inicializace platna
	 */
	public void initImage ()
	{
		// BufferedImage a propojeni s JLabelem
		image = new BufferedImage(getWidth(), getHeight(),
				BufferedImage.TYPE_INT_RGB);
		setIcon(new ImageIcon(image));
		
		// Grafika
		g = (Graphics2D)image.getGraphics();
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		g.setBackground(Color.BLACK);
		g.setColor(Color.WHITE);
		clear();
	}
	
	
	/**
	 * Pokud se komponente zmeni velikost, musi se reinicializovat.
	 */
    public void componentResized(ComponentEvent e)
    {
    	initImage();
    }
    
    
    public void clear ()
    {
		g.clearRect(0, 0, getWidth(), getHeight());
    }


    // Netreba
	public void componentHidden(ComponentEvent e) {}
	public void componentMoved(ComponentEvent e) {}
	public void componentShown(ComponentEvent e) {}
	
	
	/**
	 * TODO
	 */
}
