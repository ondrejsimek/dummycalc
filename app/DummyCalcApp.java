package app;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import evaluating.Drawer;
import evaluating.Parser;


/**
 * Hlavni okno kalkulacky. Zadna magie, proste jen JFramy, JTextFieldy,
 * BorderLayouty, KeyPressy, KeyListenery, a desitky dalsich nudnych veci...
 * 
 * @author angel333
 */
public class DummyCalcApp extends JFrame
{
	/**
	 * Nemam ani tuseni, co znamena nasledujici radek..
	 */
	private static final long serialVersionUID = 1L;

	
	/**
	 * Vytvori GUI
	 */
	public void gui ()
	{
		setBounds(0, 0, 400, 400);
		setTitle("DummyCalc");
		setLocationRelativeTo(null);
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		//setExtendedState(JFrame.MAXIMIZED_BOTH);

		// Displej
		final JTextField display = new JTextField();
		display.setEnabled(false);
		display.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 40));
		display.setForeground(Color.BLACK);
		display.setDisabledTextColor(Color.BLACK);
		display.setHorizontalAlignment(JTextField.CENTER);
		
		// Graficky displej
		final Drawer gdisplay = new Drawer();
		
		// Zadavatko
		final JTextField input = new JTextField();
		input.addKeyListener(new KeyListener() {
			public void keyTyped(KeyEvent e) {}
			public void keyReleased(KeyEvent e) {
				display.setText(String.valueOf(Parser.evaluate(input.getText())));
				
				// Graficky displej >>>>>>>>>>>>
				Double
					xMin = -10.0,
					xMax = 10.0,
					yMin = -10.0,
					yMax = 10.0;

				double xStep = (double)(Math.abs(xMax - xMin)) / (double)gdisplay.getWidth();
				double yStep = (double)(Math.abs(yMax - yMin)) / (double)gdisplay.getHeight();
				
				gdisplay.clear();
				
				Double last = null;
				
				for (int i = 0; i < gdisplay.getWidth(); i++)
				{
					double x = (xStep * i) + xMin;
					double val;
					//System.out.println(x);
					
					val = Parser.evaluate(input.getText(), x);
					// TODO nefunguje spravne..
					
					double y = ((val - yMin) / yStep);
					
					// Prvni
					if (last == null)
						last = x;
					
					// .. a kreslime....
					try {
						gdisplay.g.drawLine(i, gdisplay.getHeight() - (int)y, i, gdisplay.getHeight() - last.intValue());
					}
					catch (Exception ex) {
						// nic...
					}
					
					
					
					last = y;
				}
				gdisplay.repaint();
				// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<

			}
			public void keyPressed(KeyEvent e) {}
		});
		
		// Okno naplnit az po okraj .. 3 - 2 - 1 - TED!
		add(input, BorderLayout.NORTH);
		add(display, BorderLayout.SOUTH);
		add(gdisplay, BorderLayout.CENTER);
		
		// Okno je plne.. Zobrazit TED!
		setVisible(true);

		// asdf
		gdisplay.initImage();
	}

	
	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		new DummyCalcApp().gui();
	}
}
